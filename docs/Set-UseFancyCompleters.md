---
external help file: ps-adcompleters-help.xml
Module Name: ps-adcompleters
online version:
schema: 2.0.0
---

# Set-UseFancyCompleters

## SYNOPSIS
Enable or disable the use of more detailed completion.

## SYNTAX

```
Set-UseFancyCompleters [[-enable] <Boolean>]
```

## DESCRIPTION
With `UseFancyCompleters` set to false, only display sAMAccountName
when providing completion suggestions.

With it set to true provide the values of
`$ADCompleterConfig.FancyCompleters` for ListText and ToolTip (as
appropriate for users or groups) to their respective completion methods.

## EXAMPLES

### Example 1
```powershell
PS C:\> Set-UseFancyCompleters $true
```

Displays fancy completion.

## PARAMETERS

### -enable

```yaml
Type: Boolean
Parameter Sets: (All)
Aliases:

Required: False
Position: 0
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

## INPUTS

### None
## OUTPUTS

### System.Object
## NOTES

## RELATED LINKS
