---
external help file: ps-adcompleters-help.xml
Module Name: ps-adcompleters
online version:
schema: 2.0.0
---

# Export-ADCompleterConfig

## SYNOPSIS
Saves the current configuration state of `ps-adcompleters`.

## SYNTAX

```
Export-ADCompleterConfig
```

## DESCRIPTION
Configuration for `ps-adcompleters` is accessed via
`$ADCompleterConfig`.  Changes to this are for the current session
only unless `Export-ADCompleterConfig` is called.

## EXAMPLES

### Example 1
```powershell
PS C:\> Export-ADCompleterConfig
```

## PARAMETERS

## INPUTS

### None
## OUTPUTS

### System.Object
## NOTES

## RELATED LINKS
