# ps-adcompleters
## about_ps-adcompleters

# SHORT DESCRIPTION
Provides tab completion for AD users and groups.

# LONG DESCRIPTION
Provides tab completion for the sAMAccountNames of AD Users and groups
when using certain `*-AD*` cmdlets.

Configuration settings allow for enabling and disabling the display of
disabled user accounts as well as providing additional information for
tab-completion purposes.

Configuration is accessed through the `$ADCompleterConfig` variable
that is exported.

  * ShowDisabled :: Whether or not to display disabled user accounts
  * UseFancyCompleters :: Display basic or extended completion hints
  * group.* :: Specific format scriptblocks for groups
  * user.* :: Specific format scriptblocks for users
  * *.ToolTip :: Scriptblock defining group or user ToolTip hint
  * *.ListText :: Scriptblock defining information provided in CLI for
    hints

## Groups
Group Account completion is provided for `*-ADGroup*` cmdlets as well
as `*-ADPrincipalGroupMembership`.  Standard completion returns
sAMAccountName and displays it when providing options.

Fancy completion will complete the sAMAccountName but will provide the
following (by default) as ToolTip or ListText when selecting results:

`<DisplayName> (<sAMAccountName>) [<groupCategory>|<groupScope>]`

## Users
User Account completion is provided for `*-ADUser` and `*-ADAccount`
cmdlets as well as `*-ADPrincipalGroupMembership`.  Standard
completion returns sAMAccountName and displays it when providing
options.

If `ShowDisabled` is set to false, only currently enabled user
accounts will be displayed.

Fancy completion will complete the sAMAccountName but will provide the
following (by default) as ToolTip or ListText when selecting results:

`<EnabledFlag><DisplayName> (<sAMAccountName>)`.
