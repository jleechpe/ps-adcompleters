---
external help file: ps-adcompleters-help.xml
Module Name: ps-adcompleters
online version:
schema: 2.0.0
---

# Show-ADCompleterConfiguration

## SYNOPSIS
Prints the current configuration.

## SYNTAX

```
Show-ADCompleterConfiguration
```

## DESCRIPTION
Prints the current configuration for `ps-adcompleters`.  This is
equivalent to calling `$ADCompleterConfig` but the latter also allows
for modification.

## EXAMPLES

### Example 1

```powershell
PS C:\> Show-ADCompleterConfiguration

Name                           Value
----                           -----
ShowDisabled                   True
UseFancyCompleters             True
FancyCompleters                {group, user}
```

Prints current configuration.  Details of fancy completers are under
the `FancyCompleters` object.

## PARAMETERS

## INPUTS

### None
## OUTPUTS

### System.Object
## NOTES

## RELATED LINKS
