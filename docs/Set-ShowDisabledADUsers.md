---
external help file: ps-adcompleters-help.xml
Module Name: ps-adcompleters
online version:
schema: 2.0.0
---

# Set-ShowDisabledADUsers

## SYNOPSIS
Enable or disable the display of disabled user accounts in completion.

## SYNTAX

```
Set-ShowDisabledADUsers [[-enable] <Boolean>]
```

## DESCRIPTION
When `ShowDisabled` is set to false, do not include disabled user
accounts in list of completion results.

When set to true display them, the default fancy completion of users
will flag disabled users with `*` before their names to distinguish
them from enabled ones.

## EXAMPLES

### Example 1
```powershell
PS C:\> Set-ShowDisabledADUsers $true
```

Enables the display of disabled users.

## PARAMETERS

### -enable

```yaml
Type: Boolean
Parameter Sets: (All)
Aliases:

Required: False
Position: 0
Default value: None
Accept pipeline input: False
Accept wildcard characters: False
```

## INPUTS

### None
## OUTPUTS

### System.Object
## NOTES

## RELATED LINKS
