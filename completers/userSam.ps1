$userSam = {
    param(
        $commandName,
        $parameterName,
        $wordToComplete,
        $commandAst,
        $fakeBoundParameter
    )

    $fakeBoundParameter.remove('identity')
    $filter = "{0}*" -f $wordToComplete
    
    Get-ADUser @fakeBoundParameter -filter { samaccountname -like $filter } |
      ForEach-Object {
          $_ | Return-UserObject
      }
}

function Return-UserObject {
    [CMDLetBinding()]
    param(
        [Parameter(ValueFromPipeline)]
        $user
    )

    BEGIN {
        $completers = $script:config.FancyCompleters.user
    }

    PROCESS {
        $_ | Where-Object {
            if ($script:config.ShowDisabled) {
                $_
            } else {
                $_.enabled
            }
        } | ForEach-Object {
            if ($script:config.UseFancyCompleters) {
                $listText = & $completers.ListText
                $ToolTip  = & $completers.ToolTip
                New-Object System.Management.Automation.CompletionResult(
                    $_.samaccountname,
                    $ListText,
                    'ParameterValue',
                    $ToolTip
                )
            } else {
                New-Object System.Management.Automation.CompletionResult(
                    $_.samaccountname
                )
            }
        }
    }
}

# Enable for *-ADAccount and *-ADUser
Get-Command *-ADAccount | ForEach-Object {
    Register-ArgumentCompleter -CommandName $_ -Parameter Identity -ScriptBlock $userSam
}
Get-Command *-ADUser | ForEach-Object {
    Register-ArgumentCompleter -CommandName $_ -Parameter Identity -ScriptBlock $userSam
}
