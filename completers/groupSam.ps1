$groupSam = {
    param(
        $commandName,
        $parameterName,
        $wordToComplete,
        $commandAst,
        $fakeBoundParameter
    )

    $fakeBoundParameter.remove('identity')
    $filter = "{0}*" -f $wordToComplete
    
    Get-ADGroup @fakeBoundParameter -filter { samaccountname -like $filter } |
      ForEach-Object {
          $_ | Return-GroupObject
      }
}

function Return-GroupObject {
    [CMDLetBinding()]
    param(
        [Parameter(ValueFromPipeline)]
        $group
    )

    BEGIN {
        $completers = $script:config.FancyCompleters.group
    }

    PROCESS {
        if ($script:config.UseFancyCompleters) {
            $ListText = & $completers.ListText
            $ToolTip  = & $completers.ToolTip
            New-Object System.Management.Automation.CompletionResult(
                $_.samaccountname,
                $ListText,
                'ParameterValue',
                $ToolTip
            )
        } else {
            New-Object System.Management.Automation.Completionresult(
                $_.samaccountname
            )
        }
    }
}

# Enable for *-ADGroup*
Get-Command *-ADGroup* | ForEach-Object {
    Register-ArgumentCompleter -CommandName $_ -Parameter Identity -ScriptBlock $groupSam
}
