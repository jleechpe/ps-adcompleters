# Import completers
(Join-Path $PSScriptRoot completers/*.ps1 -resolve).ForEach{. $_}

# Default Parameter
$script:PSDefaultParameterValues = @{
    'Get-ADGroup:Properties' = "*"
    'Get-ADUser:Properties' = "*"
}

# Show Disabled Users and toggle
$script:config = Import-Configuration

function Show-ADCompleterConfiguration {
    $script:config
}

function Set-ShowDisabledADUsers {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute(
         "PSUseShouldProcessForStateChangingFunctions", "",
         Justification="Only toggles Configuration state")]
    param(
        [bool]$enable = $true
    )

    $script:config.ShowDisabled = $enable
}

function Set-UseFancyCompleters {
    [Diagnostics.CodeAnalysis.SuppressMessageAttribute(
         "PSUseShouldProcessForStateChangingFunctions", "",
         Justification = "Only toggles Configuration state")]
    param(
        [bool]$enable = $true
    )

    $script:config.UseFancyCompleters = $enable
}

function Export-ADCompleterConfig {
    $script:config | Export-Configuration
}

# Use descriptive name on export
[Diagnostics.CodeAnalysis.SuppressMessageAttribute(
     "PSUseDeclaredVarsMoreThanAssignments", "",
     Justification = "Variable for descriptive export")]
$ADCompleterConfig = $script:config
Export-ModuleMember -variable ADCompleterConfig
