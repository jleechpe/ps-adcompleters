@{
    FancyCompleters = @{
      user = @{
        ToolTip = (ScriptBlock '
                        $n = $_.displayname
                        $a = $_.samaccountname
                        $e = if ($_.enabled) { " " } else { "*" }
                        "{0}{1} ({2})" -f $e,$n,$a
                    ')
        ListText = (ScriptBlock '
                        $n = $_.displayname
                        $a = $_.samaccountname
                        $e = if ($_.enabled) { " " } else { "*" }
                        "{0}{1} ({2})" -f $e,$n,$a
                    ')
      }
      group = @{
        ToolTip = (ScriptBlock '
                        $n = $_.displayname
                        $a = $_.samaccountname
                        $t = $_.groupCategory.toString()[0]
                        $s = $_.groupScope.toString()[0]
                        "{0} ({1}) [{2}|{3}]" -f $n, $a, $t, $s
                    ')
        ListText = (ScriptBlock '
                        $n = $_.displayname
                        $a = $_.samaccountname
                        $t = $_.groupCategory.toString()[0]
                        $s = $_.groupScope.toString()[0]
                        "{0} ({1}) [{2}|{3}]" -f $n, $a, $t, $s
                    ')
      }
    }
    ShowDisabled = $True
    UseFancyCompleters = $False
}
